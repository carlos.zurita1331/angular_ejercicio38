import { Component, OnInit } from '@angular/core';
import { Form1 } from 'src/app/interface/persona.interfase';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {

  informacion:Form1={
    nombre:'',
    direccion:'',
    terminosCondiciones:true
  }

  constructor() { }

  ngOnInit(): void {
  }

  guardar():void{
    console.log("envio del submit");
    console.log(this.informacion.nombre);
    console.log(this.informacion.direccion);
    
    
  }

}
